require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @user = User.new(name: "user",
                     email: "user@example.com",
                     password: "foobar",
                     password_confirmation: "foobar")
  end

  test "should be valid" do
    assert @user.valid?
  end

  test "name should be present" do
    @user.name = " "
    assert_not @user.valid?
  end

  test "email should be present" do
    @user.email = " "
    assert_not @user.valid?
  end

  test "name should be too long" do
    @user.name = "a" * 51
    assert_not @user.valid?
  end

  test "email should be too long" do
    @user.email = "a" * 256
    assert_not @user.valid?
  end

  test "验证邮箱格式" do
    valid_addresses = %w[user@exampel.com USER@foo.COM A_US-ER@foo.bar.org
                       first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @user.email = valid_address
      assert @user.valid?, "#{valid_address.inspect} should be valid"
    end
  end

  test "拒绝重复电子邮件地址" do
    duplicate_user = @user.dup #创建重复对象
    duplicate_user.email = @user.email.upcase #不区分大小写
    @user.save
    assert_not duplicate_user.valid?
  end

  test "密码最短长度" do
    @user.password = @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end
end
