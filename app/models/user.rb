class User < ActiveRecord::Base
  before_save { self.email = email.downcase } #转换成小写
  validates :name,presence: true,length: { maximum:50 }
  VALID_EMAIL_REGEX =  /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email,
            presence: true, #存在性
            length: {maximum:255 }, #最大长度
            format: { with:VALID_EMAIL_REGEX}, #正则匹配™¡
            uniqueness: { case_sensitive: false }  #唯一且不区分大小写

  has_secure_password
  validates :password,length: { minimum:6 }
end
